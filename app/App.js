import React from 'react';
import ReactDOM, { render } from 'react-dom';
import './style/app.scss';
import force from './services/d3Force';
import { Node } from './Node';
import { Link } from './Link';

export class App extends React.Component {
  constructor(props) {
    super(props);
    // this.state = {
    //   addLinkArray: [],
    //   name: "",
    //   nodes:
    //     [
    //       {"name": "fruit", "id": 0},
    //       {"name": "apple", "id": 1},
    //       {"name": "orange", "id": 2},
    //       {"name": "banana", "id": 3}
    //     ],
    //   links:
    //     [
    //       {"source": 0, "target": 1, "id": 0},
    //       {"source": 0, "target": 2, "id": 1}
    //     ]
    // };

    const count = 70;
    const nodes = d3
      .range(0, count, 1)
      .map((value) => ({ name: "Very long name cos its cool", id: value }));

    const links = d3
      .range(0, count - 1, 1)
      .map((value) => ({ source: value % 10, target: value + 1, id: value }));

    console.log(links);
    this.state = {
      addLinkArray: [],
      name: "",
      nodes,
      links
    };
    this.handleAddNode = this.handleAddNode.bind(this);
    this.addNode = this.addNode.bind(this);
  }

  componentDidMount() {
    const data = this.state;
    force.initForce(data.nodes, data.links);
    force.tick(this);
    force.drag();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.nodes !== this.state.nodes || prevState.links !== this.state.links) {
      const data = this.state;
      force.initForce(data.nodes, data.links);
      force.tick(this);
      force.drag();
    }
  }

  handleAddNode(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  addNode(e) {
    e.preventDefault();
    this.setState(prevState => ({
      nodes: [...prevState.nodes, { name: this.state.name, id: prevState.nodes.length + 1, }], name: ''
    }));
  }

  render() {
    console.log('rerender');
    let links = this.state.links.map((link) => {
      return (
        <Link
          key={link.id}
          data={link}
        />);
    });
    let nodes = this.state.nodes.map((node) => {
      return (
        <Node
          data={node}
          name={node.name}
          key={node.id}
        />);
    });
    return (
      <div className="graph__container">
        <form className="form-addSystem" onSubmit={this.addNode.bind(this)}>
          <h4 className="form-addSystem__header">New Node</h4>
          <div className="form-addSystem__group">
            <input value={this.state.name} onChange={this.handleAddNode.bind(this)}
                   name="name"
                   className="form-addSystem__input"
                   id="name"
                   placeholder="Name"/>
            <label className="form-addSystem__label" htmlFor="title">Name</label>
          </div>
          <div className="form-addSystem__group">
            <input className="btnn" type="submit" value="add node"/>
          </div>
        </form>
        <br></br>
        <svg className="graph" width={force.width} height={force.height}>
          <g>
            {links}
          </g>
          <g>
            {nodes}
          </g>
        </svg>
      </div>
    );
  }
}

render(<App />, document.getElementById("app"));