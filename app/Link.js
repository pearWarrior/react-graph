import React from 'react';
import ReactDOM from 'react-dom';
import force from './services/d3Force';

export class Link extends React.Component {

  componentDidMount() {
    this.d3Link = d3.select(ReactDOM.findDOMNode(this))
      // .datum({ ...this.props.data, event: () => console.log('event') })
      .datum(Object.assign(this.props.data, { event: ()=> console.log(777) }))
      .call(force.enterLink);
  }

  componentDidUpdate() {
    this.d3Link.datum(this.props.data)
      .call(force.updateLink);
  }

  render() {
    return (
      <line className='link' />
    );
  }
}