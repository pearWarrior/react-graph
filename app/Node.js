import React from 'react';
import ReactDOM from 'react-dom';
import force from './services/d3Force';

export class Node extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      clicked: false
    };
    this.onClick = this.onClick.bind(this);
  }

  onClick() {
    this.setState({ clicked: true });
  };

  componentDidMount() {
    this.d3Node = d3.select(ReactDOM.findDOMNode(this))
      .datum(this.props.data)
      .call(force.enterNode)
  }

  componentDidUpdate() {
    this.d3Node.datum(this.props.data)
      .call(force.updateNode)
  }

  render() {
    return (
      <g className='node'>
        <circle onClick={this.onClick}/>
        <text>{this.state.clicked ? 'WAS CLICK' : this.props.data.name}</text>
      </g>
    );
  }
}